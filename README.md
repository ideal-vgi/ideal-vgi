# **IDEAL-VGI**

OpenStreetMap (OSM) has evolved to one of the most used geographic databases and is a prototype for volunteered geographic information (VGI). Recently, OSM has become a popular source of labeled data for the remote sensing (RS) community. However, spatial heterogeneous data quality of OSM provides challenges for the training of machine learning models. Frequently, OSM land-use and land-cover (LULC) data has thereby been taken at face value without critical reflection.

The aim of the [IDEAL-VGI](https://www.geog.uni-heidelberg.de/gis/ideal_en.html) project was to explore options to better deal with the challenges of using OSM as LULC labels for RS applications. The project has therefore developed tools and knowledge on both sides of this process: the VGI information source as well as the remote sensing community. 


## **OSM as a Data Source of Unknown Quality**

Supervised deep learning (DL) methods are found to be effective for Earth observation applications (e.g., multi-label image classification, land-cover map generation etc.) on ever-growing remote sensing (RS) image archives. The success of these methods depends on the availability of a high quantity of annotated training RS images. However, the manual collection of RS image annotations can be time consuming and costly. To overcome this, RS images can be automatically associated with multiple LULC classes (i.e., multi-labels) by using OSM tags. Due to this, large training sets can be created at zero-cost for DL-based multi-label RS image classification methods.

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/multi-label-illustration.png" />
</p>

***Figure:*** *An illustration of DL-based multi-label RS image classification.*

However, OSM tags can be outdated compared to the considered RS images or noisy because of the possible changes on the ground or annotation errors in the OSM database. This can lead to including noisy labels in training data when OSM tags are utilized as the source of training image annotations. In the framework of multi-label RS image classification, label noise is associated with missing labels or wrong labels. A missing label means that although a land use/land cover class exist in an RS image, the corresponding class label is not assigned. When a class label is assigned to RS image while the corresponding class is not present in the image, there is a wrong label.

Training deep neural networks (DNN) on a training set, which includes noisy labels due to OSM tags, may lead to learning sub-optimal model parameters and inaccurate inference performance for RS image classification. Therefore, the fitness for purpose of OSM LULC information for use by the RS community was investigated considering two label types:

 - pixel-based labels using the raw OSM LULC polygons;
 - multi-labels aggregating the available OSM information within squares of 1.2 x 1.2km.

<br>

### **OSM Quality for Pixel-Based Labels**

Depending on the quality definition and reference data, around 80% of the analysed, globally-distributed OSM polygons were of very high or perfect quality (Schott & Lautenbach, unpublished). These elements were fit for immediate use by the RS community. These findings support previous findings in the field, yet, users still face insecurity over the data quality at their specific location, timestamp and data topic and the issue of unknown quality therefor remains. To investigate this, a tool was created that will help the (automated) connection between data quality and data attributes: The OSM Element Vectorisation tool (OEV).

#### **OSM Element Vectorisation (OEV) Tool** 

The tool links over 32 data attributes considering semantic and geometric attributes, mapper and community analyses as well as incorporating external software and services. This large collection of information creates a multidimensional view on the data that extends beyond pure data quality analyses into more generic data mining. It helps the RS community as well as the VGI community to better understand their data and act accordingly. The tool was presented at FOSS4G conference 2022 in Florence [(Schott et al., 2022)](https://doi.org/10.5194/isprs-archives-XLVIII-4-W1-2022-395-2022) and the [source code](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-element-vectorisation) is available under an open license. Providing a command line interface as well as an application programming interface and a website ([https://oev.geog.uni-heidelberg.de/](https://oev.geog.uni-heidelberg.de/)), the tool enables users from all technical backgrounds to take a detailed look on OSM data.

<p align="center">
  <a href="https://www.geog.uni-heidelberg.de/md/chemgeo/geog/gis/vgiscience_heidelberg.mp4"><img src="https://www.geog.uni-heidelberg.de/md/chemgeo/geog/gis/vgiscience_heidelberg_interview_thumbnail.jpg" /></a>
</p>

***Video:*** *In a video podcast, Prof. Dr. Sven Lautenbach and Moritz Schott provide an introduction to OSM, OSM data quality and the OSM Element Vectorisation Tool.*

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/frontend.png" />
</p>

***Figure:*** *The OSM Element Vectorisation (OEV) frontend website to investigate OSM quality and data attributes of single objects in a user-friendly manner.*


### **OSM Quality for Multi-Labels**

For the second use case, an experiment in south-west Germany was implemented. The area is known to have high OSM data quality. In fact, analyses showed that 80% of labels were correct when assigning LULC class labels to small regions based on OSM using a dedicated filter mechanism. But still the remaining data issues can pose problems to deep learning models, much more so in regions where lower OSM data quality can be expected.

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/patch_overview_klein.png" />
</p>

***Figure:*** *An example of RS image patches extracted from a Sentinel-2 satellite image acquired over south-west Germany including parts of France on June 2021. The manual verification of OSM tags shows that 80% of the all labels were correct when assigning multi-labels to the patches based on the OSM database. For marked areas, green and red boxes represent correct and incorrect annotations, respectively.*

## **OSM as a Source of RS Image Labels for Training Deep Learning Models**

To address the limitations of using OSM as the source of training RS image labels, we have developed methods to: 1) first automatically detect noisy OSM tags; and 2) adjust training labels associated with noisy OSM tags for label noise robust learning of the DNN model parameters.

### **Noisy OSM Tag Detection**
Region-based RS image representations including local information and the related spatial organization of LULC classes are important for accurate detection of noisy OSM tags. However, existing DL-based multi-label RS image classification methods are not designed to provide spatial information regarding the class location.

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/noise-detection.png" />
</p>

***Figure:*** *An illustration of our noisy OSM tag detection method.*

We have developed a method that:
- utilizes an explainable artificial intelligence (XAI) algorithm to obtain visual explanations for the model predictions (i.e., self-enhancement maps).
- determines prototype vectors for each class and compare new feature vectors to their class prototype.
    - If the prototype and feature vectors are dissimilar to each other, the related class label is considered to be noisy.

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/self-enhancement-maps.png" />
</p>

***Figure:*** *An example of RS image; and its self-enhancement maps obtained on DeepLabV3+ trained under synthetic label noise rates (b) 0%; (c) 10%; (d) 20%; (e) 30%; and (f) 40%.*

<table align="center" width="90%" rules="rows">
<thead>
<tr>
<th rowpan="2">Training Set</th>
<th colspan="5"> Synthetic Label Noise Rate (SLNR) on Test Set</th>
</tr>
</thead>
<tbody>
<tr>
<td></td>
<td>0%</td>
<td>10%</td>
<td>20%</td>
<td>30%</td>
<td>40%</td>
</tr>
<tr>
<td>Abundant non-verified data (SLNR = 0%)</td>
<td>80.0</td>    
<td>88.5 </td>
<td>87.0</td>
<td>89.0</td>
<td>94.0</td>
</tr>
<tr>
<td>Abundant non-verified data (SLNR = 20%)</td>
<td>63.0</td>     
<td>72.5</td>
<td>79.0</td>
<td>82.5</td>
<td>86.5</td>
</tr>
<tr>
<td>Abundant non-verified data (SLNR = 40%)</td>
<td>0.5</td>     
<td>30.0</td>
<td>50.5</td>
<td>64.5</td>
<td>71.5</td>
</tr>
<tr>
<td>Abundant non-verified data (SLNR = 60%)</td>
<td>0.0</td>     
<td>31.0</td>
<td>55.5</td>
<td>65.5</td>
<td>74.0</td>
</tr>
<tr>
<td>Abundant non-verified data (SLNR = 80%)</td>
<td>0.0</td>     
<td>31.5</td>
<td>55.5</td>
<td>65.0</td>
<td>74.0</td>
</tr>
<tr>
<td>Small verified data</td>
<td>45.0</td>     
<td>58.5</td>
<td>67.0</td>
<td>78.5</td>
<td>84.0</td>
</tr>
</tbody>
</table>

***Table:*** *Label noise detection results in terms of accuracy (%).*

<details class="bibliography">
    <summary>References</summary>
    <ol>
        <li>Zhang X, Wei Y, Yang Y and Wu F, <em>"Rethinking localization map: Towards accurate object perception with self-enhancement maps,"</em> arXiv preprint arXiv:200605220, 2020.</li>
        <li>K.-H. Lee, X. He, L. Zhang and L. Yang, <em>"CleanNet: Transfer Learning for Scalable Image Classifier Training with Label Noise,"</em> IEEE/CVF Conference on Computer Vision and Pattern Recognition, pp. 5447-5456, 2018.
        </li>
        <li>Chen LC, Zhu Y, Papandreou G, Schroff F and Adam H, <em>"Encoder-decoder with
atrous separable convolution for semantic image segmentation"</em> European Conference on Computer Vision, 2018.
        </li>
    </ol>
</details>

<br>

### **Label Noise Robust Multi-Label Image Classification**
When a small verified subset of a training set is available, the above-mentioned method can be used to automatically find training images associated with noisy labels. Accordingly, for label-noise robust multi-label image classification, we have divided training procedure into two stages:

- In the first stage, the model parameters of the considered DNN was learned only on the small verified subset. Once this stage was finalized:
    - We first automatically identified the training images with noisy labels from the rest of training set based on the above-mentioned method. 
    - Then we automatically corrected noisy labels based on self-enhancement maps. 
- In the second stage, the considered DNN was fine-tuned on the whole training set with the corrected labels.

<table align="center" width="90%" rules="rows">
<thead>
<tr>
<th>SLNR on Training Set</th>
<th> Standard Learning </th>
<th> Label Noise Robust Learning</th>
</tr>
</thead>
<tbody>
<tr>
<td align="center">0%</td>
<td align="center">99.2</td>    
<td align="center">95.6</td>
</tr>
<tr>
<td align="center">20%</td>
<td align="center">96.8</td>     
<td align="center">89.9</td>
</tr>
<tr>
<td align="center">40%</td>
<td align="center">70.9</td>     
<td align="center">91.0</td>
</tr>
<tr>
<td align="center">60%</td>
<td align="center">66.6</td>     
<td align="center">88.3</td>
</tr>
<tr>
<td align="center">80%</td>
<td align="center">60.4</td>     
<td align="center">87.0</td>
</tr>
</tbody>
</table>

***Table:*** *Multi-label image classification results in terms of mean average precision
(%).*

## **Closing the Loop**

Automatically defining noisy OSM tags can be used to improve OSM data quality in a feedback loop. In this way, the OSM community was given the resources to correct data errors through well-known and established tools like the HOT Tasking manager: [https://tm.geog.uni-heidelberg.de/](https://tm.geog.uni-heidelberg.de/).

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/MultiTag-TaskingManager.png" />
</p>

***Figure:*** *The custom HOT Tasking Manager to feed back deep learning results to the OSM community. The image shows the concise problem description including mapping hints as well as the precise regions of identified data errors.*

The complete methods and workflow were presented at the State of the Map conference in Florence 2022 and are equally available under an open license ([Schott et al 2022](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-multitag)).


## **Extending Knowledge Across VGIScience Projects**

The insights gained into data and community analyses during the OSM data analyses were successfully applied in the context of two other projects within the VGIScience SPP dealing with user-generated content: while a data analyses of Wikidata revealed much potential for integration as well as project specific singularities ([Dsouza et al, 2022](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-wikidata-comparison)), data quality analyses were necessary to evaluate the results of a semi-automated data integration workflow combining bird observations from social media with citizen science ([Hartmann et al, 2022](https://www.sciencedirect.com/science/article/pii/S1574954122002321)).

## **Outlook**

The insights gained during the project have led us to new research directions. It became clear that the epistemologies of VGI are a much neglected field of study. Yet, they present the bases for a throrought understanding of data quality as well as for the future of this important data source. Based on our research in this project, follow-up proposals are being prepared.

We have also investigated that label-noise robust learning of DL models can be achieved within a single training procedure by identifying noisy labels during RS image representation learning based on the integration of generative and discriminative reasonings ([Sumbul et al, 2023](https://arxiv.org/abs/2212.01261)) or importance reweighting of RS images (Sumbul et al, 2023). In addition, we have also studied the effectiveness of the most informative and representative RS image triplets for learning features of RS images with multi-labels ([Sumbul et al, 2022](https://arxiv.org/abs/2105.03647)).

<p align="center">
  <img src="https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/GRID_approach_figure.png" />
</p>

***Figure:*** *An illustration of our GRID approach that jointly leverages the robustness of generative reasoning towards noisy labels and the effectiveness of discriminative reasoning on RS image representation learning ([Sumbul et al, 2023](https://arxiv.org/abs/2212.01261)).*


## **Former Team Members**

 - Tristan Kreuziger (TU Berlin)
 - [Michael Schultz](https://www.geog.uni-heidelberg.de/gis/schultz.html) (Heidelberg University)
 - Leonie Größchen (Heidelberg University)

